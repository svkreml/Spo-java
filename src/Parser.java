import java.util.ArrayList;

/**
 * Created by user on 28.03.2016.
 */
public class Parser {
    private ArrayList<Token> tokens;
    private int pos = 1;

    public Parser(ArrayList<Token> tokens) {
        this.tokens = tokens;
    }

    boolean end() {
        return (tokens.size() == pos);
    }

    Token current() {
        if (!end()) {
            return tokens.get(pos);
        }
        else {
            Token end = new Token();
            return end;
        }
    }

    void next() {
        if (!end()) {
            pos++;
        }
    }

    void error(String message) {
        System.out.println(message);
        //exit(1);
    }

    boolean isNumber() {
        Token t = current();
        return t.type == TokenType.NUMBER;
    }

    boolean isSymbol(char c) {
        Token t = current();
        return t.type == TokenType.SYMBOL && t.c == c;
    }
    boolean isUnarMinus() {
        Token t = current();
        return (tokens.get(pos-1).c=='(')&&((t.c=='-')||(tokens.get(pos-1).type==TokenType.BEGIN));
    }
///////////////////////////////////

    ExprNode binOpNode(ExprNode pLeft, char pBinop, ExprNode pRight)
    {
        return new ExprNode(pLeft, pBinop, pRight);
    }

    ExprNode numberNode(int pValue)
    {
        return new ExprNode(pValue);
    }

    ExprNode mult() {
if (isNumber()) {
            int n = current().value;
            next();
            return numberNode(n);
        }
        else if (isSymbol('(')) {
            next();
            ExprNode e = expression();
            if (!isSymbol(')')) {
                error("Missing )");
            }
            next();
            return e;
        }
        else {
            error("Unexpected symbol");
            return null;
        }
    }

    ExprNode expression() {

        ExprNode left = sum();
        while (!end()) {
            if (isSymbol('+') || isSymbol('-')) {
                char c = current().c;
                next();
                ExprNode right = sum();
                left = binOpNode(left, c, right);
            }
            else {
                break;
            }
        }
        return left;
    }

    ExprNode sum() {
        ExprNode left = mult();
        while (!end()) {
            if (isSymbol('*') || isSymbol('/')) {
                char c = current().c;
                next();
                ExprNode right = mult();
                left = binOpNode(left, c, right);
            }
            else {
                break;
            }
        }
        return left;
    }
}
