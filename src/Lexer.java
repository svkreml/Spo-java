import java.util.ArrayList;
import java.util.Map;

/**
 * Created by user on 28.03.2016.
 */
public class Lexer {

    private String str;
    int pos=0;

    public Lexer(String str) {
        this.str = str;
    }

    /////////////////////////////////////////////////////
    Map<String, Integer> vars;
    ArrayList<Token> strToTokens() {
        ArrayList<Token> tokens = new ArrayList<>();
        tokens.add(new Token(TokenType.BEGIN));
        int x = 0;
        int unar =0;
        while (x < str.length()) {
            char ch = str.charAt(x);
            if(isLetter(ch)){
                int x0 = x - unar;
                while (x < str.length() && isDigit(str.charAt(x))) {
                    x++;
                }
                tokens.add(new Token(str.substring(x0, x)));
                unar = 0;
            }
            else if (isDigit(ch)) {
                int x0 = x - unar;
                while (x < str.length() && isDigit(str.charAt(x))) {
                    x++;
                }
                tokens.add(new Token(Integer.parseInt(str.substring(x0, x))));
                unar = 0;
            }
                else if(((ch == '-')&&(str.charAt(x+1)=='('))&&((x==0))){ //||((str.charAt(x+1)=='(')) // (8-(7-6))
                    tokens.add(new Token(0));
                    tokens.add(new Token('-'));
                    x++;
                }else if((ch == '-')&&((x==0)||(str.charAt(x-1)=='('))){
                unar =1;
                x++;
            }
            else if ((ch == '(') || (ch == ')') || (ch == '+') ||
                    (ch == '-') || (ch == '*') || (ch == '/')) {
                tokens.add(new Token(ch));
                x++;
            }

            else {
                x++;
            }
        }
        return tokens;
    }

    private boolean isDigit(char ch) {
        return ch >= '0' && ch<='9';
    }
    private boolean isLetter(char ch) {
        return ch >= 'a' && ch<='z';
    }
}
