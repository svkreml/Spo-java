import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by user on 28.03.2016.
 */
public class Main {
    public static void main(String[] args) {

        Scanner s=new Scanner(System.in);
        String str = s.nextLine();
        Lexer lex = new Lexer(str);
        ArrayList<Token> tokens = lex.strToTokens();
        //Map<String, Integer> vars;
        Parser parser = new Parser(tokens);
        ExprNode e = parser.expression();
        WalkTree walkTree = new WalkTree();
        walkTree.walkTree(e);
        //walkTree.printAsmTree(e);
        System.out.println();
        int res = walkTree.calcTree(e);
        System.out.println("Result = "+res);
    }

}
