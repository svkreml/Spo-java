import java.util.Stack;

/**
 * Created by user on 28.03.2016.
 */
public class WalkTree {
    void walkTree(ExprNode node)
    {
        if (node.isNumber) {
            System.out.println(node.value);
        }
        else {
            walkTree(node.left);
            walkTree(node.right);
            System.out.println(node.binop);
        }
    }
    Stack<Integer> numbers = new Stack<>();
    private void walkTree2(ExprNode node)
    {
        if (node.isNumber) {
            numbers.push(node.value);
        }
        else {
            walkTree2(node.left);
            walkTree2(node.right);
            //int x, y;
            int x, y;
            x = numbers.pop();
            y = numbers.pop();
            switch (node.binop) {
                case '+':
                    numbers.push(x + y);
                    break;
                case '-':
                    numbers.push(y - x);
                    break;
                case '*':
                    numbers.push(y * x);
                    break;
                case '/':
                    numbers.push(y / x);
                    break;
            }

        }
    }

    int calcTree(ExprNode expr) {
        numbers.clear();
        walkTree2(expr);
        return numbers.pop();
    }

    private void asm_Tree(ExprNode node)
    {
        if (node.isNumber) {
            System.out.printf("  push "+node.value+"\n");
        }
        else {
            asm_Tree(node.left);
            asm_Tree(node.right);
            System.out.printf("  pop ebx\n  pop eax\n");
            switch (node.binop) {
                case '+':
                    System.out.printf("  add eax, ebx\n");
                    break;
                case '-':
                    System.out.printf("  sub eax, ebx\n");
                    break;
                case '*':
                    System.out.printf("  imul eax, ebx\n");
                    break;
                case '/':
                    System.out.printf("  idiv eax, ebx\n");
                    break;
            }

        }
    }

    int printAsmTree(ExprNode expr) {
        System.out.printf("section .text\n" +
                "  global _main\n" +
                "  extern _printf\n" +
                "section .data\n" +
                "  format: db \"%%d\", 10, 0\n" +
                "_main:\n");
        asm_Tree(expr);
        System.out.printf("  push eax\n"+
                "  push format\n" +
                "  call _printf\n" +
                "  add esp, 8\n"+ "ret");
        return 0;
    }
}
